# ARI Ground Temperature Data Logger

## Introduction

This project was originally started to assist in the installation and monitoring of a meteorological tower in Norman Wells, NT. It is now in the process of being adapted to being a general infrastructure monitoring unit with the goal of installing four copies at the ISSF in Inuvik, NT.

## Contact

For more information on this project, please contact wind@nwtresearch.com.

## Libraries

Required External Libraries:

For PlDuino (if used):

*  #include <Adafruit_GFX.h>
*  #include <Adafruit_SPITFT.h>
*  #include <Adafruit_SPITFT_Macros.h>
*  #include <gfxfont.h>
*  #include <Adafruit_ILI9341.h>
*  #include <PLDuino.h>

For Real Time Clock:

*  #include "RTCLib.h"