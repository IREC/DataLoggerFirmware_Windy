EESchema Schematic File Version 4
LIBS:Strain Gauge Signal Conditioner-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM741 U3
U 1 1 5C537E8A
P 5050 3000
F 0 "U3" H 5391 3046 50  0000 L CNN
F 1 "LM741" H 5391 2955 50  0000 L CNN
F 2 "" H 5100 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5200 3150 50  0001 C CNN
	1    5050 3000
	1    0    0    -1  
$EndComp
$Comp
L Potentiometer_Digital:MCP41010 U2
U 1 1 5C537F64
P 4950 1550
F 0 "U2" V 4904 1991 50  0000 L CNN
F 1 "MCP41010" V 4995 1991 50  0000 L CNN
F 2 "" H 4950 1550 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/11195c.pdf" H 4950 1550 50  0001 C CNN
	1    4950 1550
	0    1    1    0   
$EndComp
$Comp
L Potentiometer_Digital:MCP41010 U1
U 1 1 5C537FC5
P 2900 3500
F 0 "U1" H 2900 4078 50  0000 C CNN
F 1 "MCP41010" H 2900 3987 50  0000 C CNN
F 2 "" H 2900 3500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/11195c.pdf" H 2900 3500 50  0001 C CNN
	1    2900 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C538055
P 3300 2900
F 0 "R1" H 3370 2946 50  0000 L CNN
F 1 "10K" H 3370 2855 50  0000 L CNN
F 2 "" V 3230 2900 50  0001 C CNN
F 3 "~" H 3300 2900 50  0001 C CNN
	1    3300 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C5380BB
P 3300 4200
F 0 "R2" H 3370 4246 50  0000 L CNN
F 1 "10K" H 3370 4155 50  0000 L CNN
F 2 "" V 3230 4200 50  0001 C CNN
F 3 "~" H 3300 4200 50  0001 C CNN
	1    3300 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x08 J1
U 1 1 5C53815D
P 3100 6150
F 0 "J1" H 3180 6142 50  0000 L CNN
F 1 "Screw_Terminal_01x08" H 3180 6051 50  0000 L CNN
F 2 "" H 3100 6150 50  0001 C CNN
F 3 "~" H 3100 6150 50  0001 C CNN
	1    3100 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3400 3300 3050
Wire Wire Line
	3300 3600 3300 4050
$Comp
L power:GND #PWR0101
U 1 1 5C5382AA
P 4950 3850
F 0 "#PWR0101" H 4950 3600 50  0001 C CNN
F 1 "GND" H 4955 3677 50  0000 C CNN
F 2 "" H 4950 3850 50  0001 C CNN
F 3 "" H 4950 3850 50  0001 C CNN
	1    4950 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3300 4950 3500
Wire Wire Line
	5050 3300 5050 3500
Wire Wire Line
	5050 3500 4950 3500
Connection ~ 4950 3500
Wire Wire Line
	4950 3500 4950 3850
Wire Wire Line
	5150 3300 5150 3500
Wire Wire Line
	5150 3500 5050 3500
Connection ~ 5050 3500
$Comp
L power:VCC #PWR0102
U 1 1 5C538367
P 4950 2450
F 0 "#PWR0102" H 4950 2300 50  0001 C CNN
F 1 "VCC" H 4967 2623 50  0000 C CNN
F 2 "" H 4950 2450 50  0001 C CNN
F 3 "" H 4950 2450 50  0001 C CNN
	1    4950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2450 4950 2700
Text Label 7050 3000 0    50   ~ 0
SignalOut
Wire Wire Line
	2900 6450 1200 6450
Text Label 1200 6450 0    50   ~ 0
SignalOut
Wire Wire Line
	4750 3100 4250 3100
Wire Wire Line
	4250 3100 4250 3500
Wire Wire Line
	5050 1950 5050 2150
Wire Wire Line
	4950 1950 4950 2150
Wire Wire Line
	4250 2150 4250 2500
Wire Wire Line
	4850 1950 4850 2050
Text Label 1100 2050 0    50   ~ 0
SignalInput
$Comp
L power:VCC #PWR0103
U 1 1 5C538B74
P 2900 2500
F 0 "#PWR0103" H 2900 2350 50  0001 C CNN
F 1 "VCC" H 2917 2673 50  0000 C CNN
F 2 "" H 2900 2500 50  0001 C CNN
F 3 "" H 2900 2500 50  0001 C CNN
	1    2900 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C538BB7
P 2900 4700
F 0 "#PWR0104" H 2900 4450 50  0001 C CNN
F 1 "GND" H 2905 4527 50  0000 C CNN
F 2 "" H 2900 4700 50  0001 C CNN
F 3 "" H 2900 4700 50  0001 C CNN
	1    2900 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2750 3300 2650
Wire Wire Line
	3300 2650 2900 2650
Wire Wire Line
	2900 2650 2900 2500
Wire Wire Line
	2900 3100 2900 2650
Connection ~ 2900 2650
Wire Wire Line
	2900 3900 2900 4450
Wire Wire Line
	3300 4350 3300 4450
Wire Wire Line
	3300 4450 2900 4450
Connection ~ 2900 4450
Wire Wire Line
	2900 4450 2900 4700
Wire Wire Line
	4850 1150 4850 1000
Wire Wire Line
	3750 1000 4850 1000
Text Label 3750 1000 0    50   ~ 0
GainCS
Wire Wire Line
	1150 3600 2500 3600
Text Label 1150 3600 0    50   ~ 0
OffsetCS
Wire Wire Line
	2500 3500 1150 3500
Wire Wire Line
	1150 3400 2500 3400
Text Label 1150 3400 0    50   ~ 0
Clk
Text Label 1150 3500 0    50   ~ 0
MOSI
Wire Wire Line
	4950 1150 4950 900 
Wire Wire Line
	4950 900  3750 900 
Wire Wire Line
	5050 1150 5050 800 
Wire Wire Line
	5050 800  3750 800 
Text Label 3750 900  0    50   ~ 0
MOSI
Text Label 3750 800  0    50   ~ 0
Clk
$Comp
L power:VCC #PWR0105
U 1 1 5C53E482
P 1000 5700
F 0 "#PWR0105" H 1000 5550 50  0001 C CNN
F 1 "VCC" H 1017 5873 50  0000 C CNN
F 2 "" H 1000 5700 50  0001 C CNN
F 3 "" H 1000 5700 50  0001 C CNN
	1    1000 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5C53E4C5
P 1000 6100
F 0 "#PWR0106" H 1000 5850 50  0001 C CNN
F 1 "GND" H 1005 5927 50  0000 C CNN
F 2 "" H 1000 6100 50  0001 C CNN
F 3 "" H 1000 6100 50  0001 C CNN
	1    1000 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 5850 1000 5700
Wire Wire Line
	1000 5850 2900 5850
Wire Wire Line
	1000 6100 1000 5950
Wire Wire Line
	1000 5950 2900 5950
Text Label 6050 6500 0    50   ~ 0
OffsetCS
Wire Wire Line
	2900 6250 1200 6250
Wire Wire Line
	2900 6350 1200 6350
Text Label 6050 6200 0    50   ~ 0
GainCS
Text Label 1200 6250 0    50   ~ 0
Clk
Text Label 1200 6350 0    50   ~ 0
MOSI
Text Label 8550 3200 0    50   ~ 0
SignalInput
Text Label 3500 3500 0    50   ~ 0
OffsetOutput
Wire Wire Line
	3300 3500 4250 3500
$Comp
L power:GND #PWR0107
U 1 1 5C5472C0
P 8200 1700
F 0 "#PWR0107" H 8200 1450 50  0001 C CNN
F 1 "GND" H 8205 1527 50  0000 C CNN
F 2 "" H 8200 1700 50  0001 C CNN
F 3 "" H 8200 1700 50  0001 C CNN
	1    8200 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 1500 8200 1700
Text Label 8700 1600 0    50   ~ 0
OffsetOutput
Wire Notes Line
	10900 600  7800 600 
Wire Notes Line
	7800 600  7800 2500
Wire Notes Line
	7800 2500 10950 2500
Wire Notes Line
	10950 2500 10950 600 
Text Notes 7850 2450 0    50   ~ 0
Diagnostic / Calibration Outputs\n
Wire Notes Line
	700  5450 4100 5450
Wire Notes Line
	4100 5450 4100 7050
Wire Notes Line
	4100 7050 700  7050
Wire Notes Line
	700  7050 700  5450
Text Notes 750  7000 0    50   ~ 0
Board Connections\n
Wire Notes Line
	700  600  7600 600 
Wire Notes Line
	7600 650  7600 5250
Wire Notes Line
	7600 5250 700  5250
Wire Wire Line
	1100 2050 4850 2050
Wire Wire Line
	4250 2150 4950 2150
Wire Wire Line
	4250 2900 4750 2900
Wire Wire Line
	4250 2500 3550 2500
Connection ~ 4250 2500
Wire Wire Line
	4250 2500 4250 2900
Text Label 3550 2500 0    50   ~ 0
GainOutput
Text Label 8700 1700 0    50   ~ 0
GainOutput
$Comp
L power:VCC #PWR0108
U 1 1 5C5579D1
P 5900 1350
F 0 "#PWR0108" H 5900 1200 50  0001 C CNN
F 1 "VCC" H 5917 1523 50  0000 C CNN
F 2 "" H 5900 1350 50  0001 C CNN
F 3 "" H 5900 1350 50  0001 C CNN
	1    5900 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C557A16
P 4200 1650
F 0 "#PWR0109" H 4200 1400 50  0001 C CNN
F 1 "GND" H 4205 1477 50  0000 C CNN
F 2 "" H 4200 1650 50  0001 C CNN
F 3 "" H 4200 1650 50  0001 C CNN
	1    4200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1550 5900 1550
Wire Wire Line
	4550 1550 4200 1550
Wire Notes Line
	700  600  700  5250
Text Notes 800  5200 0    50   ~ 0
Strain Gauge Signal Conditioner\n
Text Notes 7050 6650 0    50   ~ 0
Signal Conditioner Board for Norman Wells Wind Project
Wire Wire Line
	5900 1550 5900 1350
Wire Wire Line
	4200 1550 4200 1650
Wire Wire Line
	5350 3000 6300 3000
Wire Wire Line
	6300 2150 6300 3000
Connection ~ 6300 3000
Wire Wire Line
	6300 3000 7050 3000
Wire Wire Line
	9550 1700 8700 1700
Wire Wire Line
	8200 1500 9550 1500
Wire Wire Line
	9550 1600 8700 1600
$Comp
L Connector:Screw_Terminal_01x04 J2
U 1 1 5C56607E
P 9750 1600
F 0 "J2" H 9830 1592 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 9830 1501 50  0000 L CNN
F 2 "" H 9750 1600 50  0001 C CNN
F 3 "~" H 9750 1600 50  0001 C CNN
	1    9750 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R 100K1
U 1 1 5C5B196A
P 5650 2150
F 0 "100K1" V 5443 2150 50  0000 C CNN
F 1 "R" V 5534 2150 50  0000 C CNN
F 2 "" V 5580 2150 50  0001 C CNN
F 3 "~" H 5650 2150 50  0001 C CNN
	1    5650 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 2150 6300 2150
Wire Wire Line
	5500 2150 5050 2150
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5C5BB92A
P 9550 3200
F 0 "J3" H 9630 3192 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 9630 3101 50  0000 L CNN
F 2 "" H 9550 3200 50  0001 C CNN
F 3 "~" H 9550 3200 50  0001 C CNN
	1    9550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5C5BC97C
P 8850 3500
F 0 "#PWR01" H 8850 3250 50  0001 C CNN
F 1 "GND" H 8855 3327 50  0000 C CNN
F 2 "" H 8850 3500 50  0001 C CNN
F 3 "" H 8850 3500 50  0001 C CNN
	1    8850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3300 8850 3300
Wire Wire Line
	8850 3300 8850 3500
Wire Wire Line
	9350 3200 8550 3200
Wire Notes Line
	7800 2750 7800 4400
Wire Notes Line
	7800 4400 11000 4400
Wire Notes Line
	11000 4400 11000 2750
Wire Notes Line
	11000 2750 7800 2750
Text Notes 7850 4300 0    50   ~ 0
Input Terminal
$Comp
L 74xx:7400 U?
U 1 1 5C5C50F6
P 5550 6200
F 0 "U?" H 5550 6525 50  0000 C CNN
F 1 "7400" H 5550 6434 50  0000 C CNN
F 2 "" H 5550 6200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn7400" H 5550 6200 50  0001 C CNN
	1    5550 6200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5C5D61B7
P 5150 5900
F 0 "#PWR?" H 5150 5750 50  0001 C CNN
F 1 "VCC" H 5167 6073 50  0000 C CNN
F 2 "" H 5150 5900 50  0001 C CNN
F 3 "" H 5150 5900 50  0001 C CNN
	1    5150 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5900 5150 6100
Wire Wire Line
	5150 6100 5250 6100
Wire Wire Line
	4550 6300 5100 6300
Wire Wire Line
	6050 6500 5100 6500
Wire Wire Line
	5100 6500 5100 6300
Connection ~ 5100 6300
Wire Wire Line
	5100 6300 5250 6300
Wire Wire Line
	6050 6200 5850 6200
Wire Notes Line
	6650 7050 6650 5450
Wire Notes Line
	6650 5450 4400 5450
Wire Notes Line
	4400 5450 4400 7050
Wire Notes Line
	4400 7050 6650 7050
Text Label 4550 6300 0    50   ~ 0
CS
Wire Wire Line
	2900 6050 1200 6050
Text Label 1200 6050 0    50   ~ 0
CS
$EndSCHEMATC
