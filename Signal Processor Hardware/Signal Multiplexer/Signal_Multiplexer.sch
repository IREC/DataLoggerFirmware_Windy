EESchema Schematic File Version 4
LIBS:Signal_Multiplexer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4xxx:4051 U1
U 1 1 5BBACCB9
P 4850 2350
F 0 "U1" H 5391 2396 50  0000 L CNN
F 1 "4051" H 5391 2305 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 4850 2350 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4051bms-52bms-53bms.pdf" H 4850 2350 50  0001 C CNN
	1    4850 2350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0109
U 1 1 5BBAE01D
P 4850 1250
F 0 "#PWR0109" H 4850 1100 50  0001 C CNN
F 1 "VCC" H 4867 1423 50  0000 C CNN
F 2 "" H 4850 1250 50  0001 C CNN
F 3 "" H 4850 1250 50  0001 C CNN
	1    4850 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1250 4850 1450
$Comp
L power:GND #PWR0110
U 1 1 5BBAE2A5
P 4850 3450
F 0 "#PWR0110" H 4850 3200 50  0001 C CNN
F 1 "GND" H 4855 3277 50  0000 C CNN
F 2 "" H 4850 3450 50  0001 C CNN
F 3 "" H 4850 3450 50  0001 C CNN
	1    4850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3250 4850 3400
$Comp
L Connector:Screw_Terminal_01x08 J1
U 1 1 5BBB64AE
P 2450 2150
F 0 "J1" H 2370 1525 50  0000 C CNN
F 1 "Screw_Terminal_01x08" H 2370 1616 50  0000 C CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x08_P3.50mm_Horizontal" H 2450 2150 50  0001 C CNN
F 3 "~" H 2450 2150 50  0001 C CNN
	1    2450 2150
	-1   0    0    1   
$EndComp
Wire Notes Line
	6300 850  6300 4950
Wire Notes Line
	1800 4950 1800 850 
Text Notes 5400 4900 0    50   ~ 0
8 to 1 Analogue Mux\n
Wire Notes Line
	1800 5100 5750 5100
Text Notes 5150 7300 0    50   ~ 0
MCU Interface\n
Wire Wire Line
	4950 3250 4950 3400
Wire Wire Line
	4950 3400 4850 3400
Connection ~ 4850 3400
Wire Wire Line
	4850 3400 4850 3450
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5C0C2832
P 7700 5600
F 0 "J3" H 7780 5592 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 7780 5501 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x02_P3.50mm_Vertical" H 7700 5600 50  0001 C CNN
F 3 "~" H 7700 5600 50  0001 C CNN
	1    7700 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5600 7050 5600
Wire Wire Line
	7500 5700 7050 5700
$Comp
L power:GND #PWR0112
U 1 1 5C0C5352
P 7050 5700
F 0 "#PWR0112" H 7050 5450 50  0001 C CNN
F 1 "GND" H 7055 5527 50  0000 C CNN
F 2 "" H 7050 5700 50  0001 C CNN
F 3 "" H 7050 5700 50  0001 C CNN
	1    7050 5700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0113
U 1 1 5C0C53A2
P 7050 5600
F 0 "#PWR0113" H 7050 5450 50  0001 C CNN
F 1 "VCC" H 7067 5773 50  0000 C CNN
F 2 "" H 7050 5600 50  0001 C CNN
F 3 "" H 7050 5600 50  0001 C CNN
	1    7050 5600
	1    0    0    -1  
$EndComp
Wire Notes Line
	6400 5100 10000 5100
Wire Notes Line
	10000 5100 10000 6300
Wire Notes Line
	10000 6300 6400 6300
Wire Notes Line
	6400 6300 6400 5100
Text Notes 9350 6250 0    50   ~ 0
Power Hookup\n
$Comp
L Connector:Screw_Terminal_01x08 J4
U 1 1 5C0D93AB
P 2450 3250
F 0 "J4" H 2370 2625 50  0000 C CNN
F 1 "Screw_Terminal_01x08" H 2370 2716 50  0000 C CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x08_P3.50mm_Horizontal" H 2450 3250 50  0001 C CNN
F 3 "~" H 2450 3250 50  0001 C CNN
	1    2450 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 1850 3050 1850
Wire Wire Line
	2650 2050 3050 2050
Wire Wire Line
	2650 2250 3050 2250
Wire Wire Line
	2650 2450 3050 2450
Wire Wire Line
	2650 2950 3050 2950
Wire Wire Line
	2650 3150 3050 3150
Wire Wire Line
	2650 3350 3050 3350
Wire Wire Line
	2650 3550 3050 3550
Wire Wire Line
	4100 2650 4100 4100
Wire Wire Line
	4150 2750 4150 4200
Wire Wire Line
	4100 2650 4350 2650
Wire Wire Line
	4150 2750 4350 2750
Wire Wire Line
	4350 2850 4200 2850
Wire Wire Line
	4200 2850 4200 4300
Wire Wire Line
	4250 4400 4250 2950
Wire Wire Line
	4250 2950 4350 2950
$Comp
L Device:R R1
U 1 1 5C102000
P 3100 6000
F 0 "R1" H 3170 6046 50  0000 L CNN
F 1 "470" H 3170 5955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3030 6000 50  0001 C CNN
F 3 "~" H 3100 6000 50  0001 C CNN
	1    3100 6000
	1    0    0    -1  
$EndComp
Wire Notes Line
	5750 5100 5750 7400
Wire Notes Line
	1800 5100 1800 7400
$Comp
L Device:R R2
U 1 1 5C10F993
P 3350 6000
F 0 "R2" H 3420 6046 50  0000 L CNN
F 1 "470" H 3420 5955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3280 6000 50  0001 C CNN
F 3 "~" H 3350 6000 50  0001 C CNN
	1    3350 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C10F9EB
P 3600 6000
F 0 "R3" H 3670 6046 50  0000 L CNN
F 1 "470" H 3670 5955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3530 6000 50  0001 C CNN
F 3 "~" H 3600 6000 50  0001 C CNN
	1    3600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6450 4350 6450
Wire Wire Line
	3050 1850 3050 2050
Connection ~ 3050 2050
Wire Wire Line
	3050 2050 3050 2250
Connection ~ 3050 2250
Wire Wire Line
	3050 2250 3050 2450
Connection ~ 3050 2450
Wire Wire Line
	3050 2450 3050 2950
Connection ~ 3050 2950
Wire Wire Line
	3050 2950 3050 3150
Connection ~ 3050 3150
Wire Wire Line
	3050 3150 3050 3350
Connection ~ 3050 3350
Wire Wire Line
	3050 3350 3050 3550
Wire Wire Line
	2650 1750 4350 1750
Wire Wire Line
	2650 1950 3150 1950
Wire Wire Line
	3150 1950 3150 1850
Wire Wire Line
	3150 1850 4350 1850
Wire Wire Line
	3250 2150 3250 1950
Wire Wire Line
	2650 2150 3250 2150
Wire Wire Line
	3250 1950 4350 1950
Wire Wire Line
	3350 2350 3350 2050
Wire Wire Line
	2650 2350 3350 2350
Wire Wire Line
	3350 2050 4350 2050
Wire Wire Line
	3450 2150 3450 2850
Wire Wire Line
	2650 2850 3450 2850
Wire Wire Line
	3450 2150 4350 2150
Wire Wire Line
	3550 2250 3550 3050
Wire Wire Line
	2650 3050 3550 3050
Wire Wire Line
	3550 2250 4350 2250
Wire Wire Line
	3650 2350 3650 3250
Wire Wire Line
	2650 3250 3650 3250
Wire Wire Line
	3650 2350 4350 2350
Wire Wire Line
	3750 2450 3750 3450
Wire Wire Line
	2650 3450 3750 3450
Wire Wire Line
	3750 2450 4350 2450
Text Label 2200 6550 0    50   ~ 0
ChanSelA
Text Label 2200 6650 0    50   ~ 0
ChanSelB
Text Label 2200 6750 0    50   ~ 0
ChanSelC
Wire Wire Line
	3200 4200 4150 4200
Wire Wire Line
	3200 4300 4200 4300
Wire Wire Line
	3200 4400 4250 4400
Text Label 3200 4200 0    50   ~ 0
ChanSelA
Text Label 3200 4300 0    50   ~ 0
ChanSelB
Text Label 3200 4400 0    50   ~ 0
ChanSelC
Text Label 5850 1750 0    50   ~ 0
MuxOut
Wire Wire Line
	2200 6350 4350 6350
Text Label 2200 6350 0    50   ~ 0
MuxOut
Wire Notes Line
	1800 850  6300 850 
Wire Notes Line
	1800 4950 6300 4950
$Comp
L power:GND #PWR01
U 1 1 5C5A43A6
P 3000 4150
F 0 "#PWR01" H 3000 3900 50  0001 C CNN
F 1 "GND" H 3005 3977 50  0000 C CNN
F 2 "" H 3000 4150 50  0001 C CNN
F 3 "" H 3000 4150 50  0001 C CNN
	1    3000 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4100 3000 4150
Wire Wire Line
	3000 4100 4100 4100
$Comp
L Device:R 22K1
U 1 1 5C59E9B1
P 5700 2650
F 0 "22K1" H 5770 2696 50  0000 L CNN
F 1 "R" H 5770 2605 50  0000 L CNN
F 2 "" V 5630 2650 50  0001 C CNN
F 3 "~" H 5700 2650 50  0001 C CNN
	1    5700 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C59E9FE
P 5700 3000
F 0 "#PWR02" H 5700 2750 50  0001 C CNN
F 1 "GND" H 5705 2827 50  0000 C CNN
F 2 "" H 5700 3000 50  0001 C CNN
F 3 "" H 5700 3000 50  0001 C CNN
	1    5700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2500 5700 2350
Wire Wire Line
	5700 2800 5700 3000
$Comp
L Connector:Screw_Terminal_01x06 J2
U 1 1 5C5A4DDF
P 4550 6550
F 0 "J2" H 4630 6542 50  0000 L CNN
F 1 "Screw_Terminal_01x06" H 4630 6451 50  0000 L CNN
F 2 "" H 4550 6550 50  0001 C CNN
F 3 "~" H 4550 6550 50  0001 C CNN
	1    4550 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1750 5700 1750
$Comp
L Device:Jumper JP1
U 1 1 5C5B4477
P 5700 2050
F 0 "JP1" V 5654 2177 50  0000 L CNN
F 1 "Jumper" V 5745 2177 50  0000 L CNN
F 2 "" H 5700 2050 50  0001 C CNN
F 3 "~" H 5700 2050 50  0001 C CNN
	1    5700 2050
	0    1    1    0   
$EndComp
Connection ~ 5700 1750
Wire Wire Line
	5700 1750 5850 1750
Wire Wire Line
	3050 1850 3050 1500
Connection ~ 3050 1850
$Comp
L power:VCC #PWR?
U 1 1 5C674B4A
P 3050 1500
F 0 "#PWR?" H 3050 1350 50  0001 C CNN
F 1 "VCC" H 3067 1673 50  0000 C CNN
F 2 "" H 3050 1500 50  0001 C CNN
F 3 "" H 3050 1500 50  0001 C CNN
	1    3050 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6550 3100 6550
Wire Wire Line
	2200 6650 3350 6650
Wire Wire Line
	2200 6750 3600 6750
$Comp
L power:GND #PWR?
U 1 1 5C67C1FC
P 4000 7050
F 0 "#PWR?" H 4000 6800 50  0001 C CNN
F 1 "GND" H 4005 6877 50  0000 C CNN
F 2 "" H 4000 7050 50  0001 C CNN
F 3 "" H 4000 7050 50  0001 C CNN
	1    4000 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6450 4000 7050
$Comp
L power:VCC #PWR?
U 1 1 5C6835FE
P 3100 5500
F 0 "#PWR?" H 3100 5350 50  0001 C CNN
F 1 "VCC" H 3117 5673 50  0000 C CNN
F 2 "" H 3100 5500 50  0001 C CNN
F 3 "" H 3100 5500 50  0001 C CNN
	1    3100 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 5500 3100 5750
Wire Wire Line
	3100 6150 3100 6550
Connection ~ 3100 6550
Wire Wire Line
	3100 6550 4350 6550
Wire Wire Line
	3350 6650 3350 6150
Connection ~ 3350 6650
Wire Wire Line
	3350 6650 4350 6650
Wire Wire Line
	3600 6150 3600 6750
Connection ~ 3600 6750
Wire Wire Line
	3600 6750 4350 6750
Wire Wire Line
	3350 5850 3350 5750
Wire Wire Line
	3350 5750 3100 5750
Connection ~ 3100 5750
Wire Wire Line
	3100 5750 3100 5850
Wire Wire Line
	3600 5850 3600 5750
Wire Wire Line
	3600 5750 3350 5750
Connection ~ 3350 5750
Wire Notes Line
	5750 7400 1800 7400
$EndSCHEMATC
