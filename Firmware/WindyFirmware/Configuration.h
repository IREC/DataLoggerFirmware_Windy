
//Configuration.h for WindyFirmware

//--------------------------------------------------------
// DATA LOGGER - Feature Configuration
//--------------------------------------------------------

/*
  * Device Selection
*/
//#define PLDUINO
#define CUSTOM_BOARD

/*
  * User Interface
*/
//#define PLDUINO_SCREEN_MODULE //Uncomment to use PLDuino Screen
#define VERBOSE_SERIAL

//Uncomment below if ADC lines have 8 to 1 multiplexers in front of them.
//  -Each sensor bank is assumed to have 8 unique channels.
//  -If no mux is installed, the same value will be read for every channel.
//  -Sensor bank count is defined in conditionals below.
#define EIGHT_TO_ONE_SENSOR_BANKS

//Uncomment below if some data is ready directly 1 to 1 from an ADC channel
#define DIRECT_MEASURE //Not Yet Implemented (Battery Voltage Monitor)

//Uncomment below if external signal processors are being used
//  -Not completely implemented
//#define EXTERNAL_SIGNAL_PROCESSOR

//--------------------------------------------------------
// DATA LOGGER - Electrical Configuration
//--------------------------------------------------------

/*
  *Input Pin Definitions
*/


/*
  *Output Pin Definitions
*/

//
// CONDITIONALS
//--------------------------------------------------------

#ifdef PLDUINO
  #include <Adafruit_GFX.h>
  #include <Adafruit_SPITFT.h>
  #include <Adafruit_SPITFT_Macros.h>
  #include <gfxfont.h>
  #include <Adafruit_ILI9341.h>
  #include <PLDuino.h>
  using namespace PLDuino;
  Adafruit_ILI9341 tft = Adafruit_ILI9341(LCD_CS, LCD_DC, LCD_RST);

  #define PLDUINO_SCREEN_MODULE
  //Screen Layout
  #define margin 5
  #define rowOne 5
  #define rowTwo 30
  #define rowThree 55
  #define rowFour 80
#endif
#ifdef CUSTOM_BOARD
  #define SD_CS_PIN 10
#endif

#ifdef EIGHT_TO_ONE_SENSOR_BANKS
  #define SENSOR_BANK_COUNT 1
  const uint8_t sensorBanks[SENSOR_BANK_COUNT] = {0};
  #define MUX_CHANNEL_SEL_A 2 // LSB
  #define MUX_CHANNEL_SEL_B 3 //
  #define MUX_CHANNEL_SEL_C 4 // MSB
#endif //EIGHT_TO_ONE_SENSOR_BANKS

#ifdef DIRECT_MEASURE
  const uint8_t directMeasurements[] = {3};
#endif //DIRECT_MEASURE

#ifdef EXTERNAL_SIGNAL_PROCESSOR
  #define SIGNAL_PROCESSOR_PWR_PIN 42 //
#endif //EXTERNAL_SIGNAL_PROCESSOR

//--------------------------------------------------------
// DATA LOGGER - Behavioural Parameters
//--------------------------------------------------------

//Uncomment below the desired measurement interval
#define HOURLY_DATA
//#define MINUTE_DATA
//#define FOUR_HOUR_DATA

//Uncomment below which data files to write
#define WRITE_RAW_DATA_FILE
//#define WRITE_PROCESSED_DATA_FILE //Conversion Chart Not Yet Implemented.

//
// CONDITIONALS
//--------------------------------------------------------


/* Digital Outputs Pin Assignments

*/


/* Analog Input Pin Assignments

*/


/*
	* Runtime Parameters
*/

//Uncomment the calculation method of temperature
#define TEMP_LOOK_UP_TABLE
//#define TEMP_LOG_CALC //Not implemented yet



/*
  * Temperature Lookup Table
  -Defined for a 20K pull down reference resistor
  -Defined for a 10K NTC Thermistor with a Beta = 3435K
  -Defined for a 12VDC supply
*/
#define lookUpTableSize 21
const int temperatures[lookUpTableSize] = {40,35,30,25,20,15,10,5,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-20,-30};
const int rawTemperatureValues[lookUpTableSize]  = {954,914,869,819,763,703,640,572,504,490,477,463,450,436,422,409,396,384,370,251,157};
