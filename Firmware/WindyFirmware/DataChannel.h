#include <Arduino.h>
#include "Configuration.h"

#ifndef DATACHANNEL_H
#define DATACHANNEL_H

enum ChannelTypes
{
  NORMAL,
  BANK
};

class DataChannel 
{
  public:
  // * CONSTRUCTOR
  DataChannel(uint8_t,uint8_t,uint8_t,uint8_t);
  private:
  // * DATA
  uint8_t pin; //The actual ADC pin it will read from.
  uint8_t selA; //For storing it's virtual pin location.
  uint8_t selB;
  uint8_t selC;
  uint8_t raw; //The raw data read from the ADC.
  double processed; //The raw data once passed through the transfer function.
  ChannelTypes type;
  // * FUNCTIONS
  transferFunction();
  toString();
  read();
}

#endif