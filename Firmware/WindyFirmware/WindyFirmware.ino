#include <SPI.h>
#include <Wire.h>
#include "RTClib.h"
#include <SD.h>
#include "Configuration.h"
#include "LowPower.h"

RTC_DS1307 rtc;
DateTime now;
DateTime previous;

File masterFile;
String dataString;

String dataFileName = "PROCCESSED.DAT";
String rawDataFileName = "RAW.DAT";
String logFileName = "LOG.TXT";
String timeString = "";

void setup() {
  Serial.begin(9600);
  #ifdef PLDUINO
    PLDuino::init();
  #endif
  #ifndef PLDUINO
    pinMode(MUX_CHANNEL_SEL_A,OUTPUT);
    pinMode(MUX_CHANNEL_SEL_B,OUTPUT);
    pinMode(MUX_CHANNEL_SEL_C,OUTPUT);
  #endif
  //Initialize RTC
  while(!rtc.begin()) {
    consoleLog("Failed to start Wire");
    delay(2000);
  }
  while(!rtc.isrunning()) {
    consoleLog("RTC is not running!");
    now = rtc.now();
    Serial.print(now.hour());
    Serial.print(":");
    Serial.print(now.minute());
    Serial.print(":");
    Serial.println(now.second());
    delay(2000);
  }
  now = rtc.now();
  consoleLog("Data Logger - Code Name: Frosty");
  consoleLog(generateTimeStamp());
  //Pin Initialization
  #ifdef EXTERNAL_SIGNAL_PROCESSOR
    digitalWrite(signalConditionerCS,LOW);
    digitalWrite(signalConditionerCLK,LOW);
    digitalWrite(signalConditionerMOSI,HIGH);
  #endif //EXTERNAL_SIGNAL_PROCESSOR
  #ifdef EIGHT_TO_ONE_SENSOR_BANKS
    digitalWrite(MUX_CHANNEL_SEL_A,LOW);
    digitalWrite(MUX_CHANNEL_SEL_B,LOW);
    digitalWrite(MUX_CHANNEL_SEL_C,LOW);
  #endif
  #ifdef PLDUINO
    //Setup screen and rotation
    enableLCD();
    tft.begin();
    tft.setRotation(3);
    clearScreen();
    consoleLog("PLDuino Initialized and LCD Enabled");
    //Test Screen
    drawToScreen(1,"Data Logger - Windy");
    drawToScreen(2,"Starting...");
    while(SD.begin(PLDuino::SD_CS) == false) {
      //Print some sort of error to the screen.
      consoleLog("Failed to Initialize SD Card");
      drawToScreen(4,"SD Card Failed");
      delay(2000);
    }
  clearScreen();
  #endif //PLDUINO
  #ifdef CUSTOM_BOARD
    while(SD.begin(SD_CS_PIN) == false) {
      //Print some sort of error to the screen.
      consoleLog("Failed to Initialize SD Card");
      delay(2000);
    }
  #endif //CUSTOM_BOARD
  //Setup first reading from RTC
  previous = rtc.now();
  //Initialize SD Card
  consoleLog("SD Card Initialized");
  fileLog("Data Logger Started");
  #ifdef PLDUINO
    drawToScreen(1,"Data Logger - Windy");
    drawToScreen(2,"Start Complete");
  #endif //PLDUINO
}
void loop() {
  LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
  #ifdef FOUR_HOUR_DATA
    if(now.hour() != previous.hour() && ((int)now.hour())%4 == 0) {
      consoleLog("Writing 4 Hour Data");
      #ifdef WRITE_PROCESSED_DATA_FILE
        appendToFile(dataFileName, generateTimeStamp());
        appendLineToFile(dataFileName, generateData());
      #endif //WRITE_PROCESSED_DATA_FILE
      #ifdef WRITE_RAW_DATA_FILE
        appendToFile(rawDataFileName, generateTimeStamp());
        appendLineToFile(rawDataFileName, generateRawData());
      #endif //WRITE_RAW_DATA_FILE
    }
  #endif //FOUR_HOUR_DATA
  #ifdef HOURLY_DATA
		if(now.hour() != previous.hour()) {
      consoleLog("Writing Hourly Data");
      #ifdef WRITE_PROCESSED_DATA_FILE
        appendToFile(dataFileName, generateTimeStamp());
        appendLineToFile(dataFileName, generateData());
      #endif //WRITE_PROCESSED_DATA_FILE
      #ifdef WRITE_RAW_DATA_FILE
        appendToFile(rawDataFileName, generateTimeStamp());
        appendLineToFile(rawDataFileName, generateRawData());
      #endif //WRITE_RAW_DATA_FILE
		}
	#endif //HOURLY_DATA
  #ifdef MINUTE_DATA
		if(now.minute() != previous.minute()) {
      consoleLog("Writing Minute Data");
      #ifdef WRITE_PROCESSED_DATA_FILE
        appendToFile(dataFileName, generateTimeStamp());
        appendLineToFile(dataFileName, generateData());
      #endif //WRITE_PROCESSED_DATA_FILE
      #ifdef WRITE_RAW_DATA_FILE
        appendToFile(rawDataFileName, generateTimeStamp());
        appendLineToFile(rawDataFileName, generateRawData());
      #endif //WRITE_RAW_DATA_FILE
		}
	#endif //MINUTE_DATA
	previous = now; //change over times for the next pass through
  #ifdef VERBOSE_SERIAL
  //Dump Sensor Data to Serial Port
  consoleLog(generateTimeStamp());
  consoleLog(generateRawData());
  Serial.flush();
  #endif //VERBOSE_SERIAL
}
void appendLineToFile(String fileName, String message) {
	masterFile = SD.open(fileName,FILE_WRITE);
	masterFile.println(message);
	masterFile.close();
}
void appendToFile(String fileName, String message) {
  masterFile = SD.open(fileName,FILE_WRITE);
  masterFile.print(message);
  masterFile.close();
}
void fileLog(String message) {
  String formatedMessage = generateTimeStamp() + " -> " + message;
  appendLineToFile(logFileName, formatedMessage);
}
/* generateProcessedData() NEEDS TO BE UPDATED AND VERIFIED
  -Returns a single string containing converted temperatures from all channels deliminated with a space.
*/
String generateProcessedData() {
  String dataString = " ";
  for(int i = 0; i < 8; i++) {
    setChannel(i);
    delay(10);
    for(int i = 0; i < sizeof(sensorBanks); i++) {
      dataString += getTemperature((int)analogRead(sensorBanks[i]));
      dataString += " ";
    }
    if(i!=7) {
      dataString += " - ";
    }
  }
  setChannel(0);
}
/* generateRawData()
  -Returns a single string containing raw ADC data from all channels deliminated with a space.
*/
String generateRawData() {
  String dataString = " ";
  #ifdef EIGHT_TO_ONE_SENSOR_BANKS
  for(int i = 0; i < 8; i++) {
    setChannel(i);
    delay(5);
    for(int i = 0; i < SENSOR_BANK_COUNT; i++) {
      dataString += (int)analogRead(sensorBanks[i]);
      dataString += " ";
    }
    if(i!=7) {
      dataString += " ";
    }
  }
  #endif //EIGHT_TO_ONE_SENSOR_BANKS
  setChannel(0);
  return dataString;
}
/* generateTimeStamp()
  -Returns a string containing the current timestamp.
*/
String generateTimeStamp() {
	now = rtc.now();
  timeString = "";
  timeString += now.year();
  timeString += " ";
  timeString += now.month();
  timeString += " ";
  timeString += now.day();
  timeString += " ";
  timeString += now.hour();
  timeString += ":";
  timeString += now.minute();
  timeString += ":";
  timeString += now.second();
  return timeString;
}
/* getTemperature(rawADCValue) NEEDS TO BE UPDATED AND VERIFIED
  -Returns the temperature in degrees Celsius.
  -Accepts a raw ADC reading value (i.e. 0-1023).
  -Uses the temperature lookup table declared in config file.
*/
int getTemperature(uint16_t rawValue) {
  int lowerBound = 0;
  if(rawValue < rawTemperatureValues[0]) {
    return temperatures[0];
  }
  else if(rawValue < rawTemperatureValues[lookUpTableSize]) {
    return temperatures[lookUpTableSize];
  }
  for(int i = 0; i < lookUpTableSize-1; i++) {
    if(rawValue > rawTemperatureValues[i] && rawValue < rawTemperatureValues[i+1]) {
      lowerBound = i;
    }
  }
  return map(rawValue,rawTemperatureValues[lowerBound],rawTemperatureValues[lowerBound+1],temperatures[lowerBound],temperatures[lowerBound+1]);
}
/* setChannel()
  -Accepts a channel (0-7) and sets the correct output pins for the Mux Chips.
*/
void setChannel(uint8_t channel) {
	//A is LSB
	//C id MSB
  if(channel < 0 || channel > 7) {
    return;
  }
	if(channel < 4)	{
		digitalWrite(MUX_CHANNEL_SEL_C,LOW);
	}
	else {
		digitalWrite(MUX_CHANNEL_SEL_C,HIGH);
	}
	if(channel == 2 || channel == 3 || channel == 6 || channel == 7) {
		digitalWrite(MUX_CHANNEL_SEL_B,HIGH);
	}
	else {
		digitalWrite(MUX_CHANNEL_SEL_B,LOW);
	}
	if(channel%2==1) {
		digitalWrite(MUX_CHANNEL_SEL_A,HIGH);
	}
	else {
		digitalWrite(MUX_CHANNEL_SEL_A,LOW);
  }
  delay(100);
}
/*  consoleLog(String message)
  -Writes the message to the selected comms channel along with the timestamp
  -A wrapper class for Serial.println
  -Intended to control the style and format of all comms
  -Can be expanded to select what interface communicates the data (i.e. i2c,SPI,UART)
*/
void consoleLog(String message) {
  Serial.print(millis());
  Serial.print(": ");
  Serial.println(message);
}
#ifdef EXTERNAL_SIGNAL_PROCESSOR
  void writeToGainPot(uint8_t value) {
    digitalWrite(signalConditionerCS, LOW);
    shiftOut(signalConditionerMOSI,signalConditionerCLK,ditigalPotWriteDirection,~0x00);
    shiftOut(signalConditionerMOSI,signalConditionerCLK,ditigalPotWriteDirection,~value);
    digitalWrite(signalConditionerCS, HIGH);
  }
  void writeToOffsetPot(uint8_t value) {
    digitalWrite(signalConditionerCS, HIGH);
    shiftOut(signalConditionerMOSI,signalConditionerCLK,ditigalPotWriteDirection,~0x00);
    shiftOut(signalConditionerMOSI,signalConditionerCLK,ditigalPotWriteDirection,~value);
    digitalWrite(signalConditionerCS, LOW);
    digitalWrite(signalConditionerCS, LOW);
  }
#endif //EXTERNAL_SIGNAL_PROCESSOR
#ifdef PLDUINO
  void drawToScreen(uint8_t row, String data) {
    if(row ==1)
      tft.setCursor(margin,rowOne);
    else if(row ==2)
      tft.setCursor(margin,rowTwo);
    else if(row ==3)
      tft.setCursor(margin,rowThree);
    else if(row ==4)
      tft.setCursor(margin,rowFour);
    tft.setTextColor(ILI9341_WHITE);
    tft.setTextSize(2);
    tft.println(data);
    digitalWrite(PLDuino::LCD_CS, HIGH); // Test
  }
  void clearScreen() {
    tft.fillScreen(ILI9341_BLACK); //Clears screen
  }
#endif PLDUINO
